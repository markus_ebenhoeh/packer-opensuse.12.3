# Packer-OpenSuse.12.3

This [Packer][1] template is setting up a very basic OpenSuSE 12.3 server installation for Vagrant 
using [VMWare workstation][3] and [VirtualBox][2] providers.

The only user is verpacker and neither verpacker nor root nor anyother users have a valid password set.
The only way to login is by using the keys that are also set up as part of the template.


## How To

I ran this on an OpenSuSE 12.2 desktop.

1. install [VirtualBox][2]  (tested with 4.1.22-1.14.6.x86_64 )
2. install [VMWare Workstation][3] (tested with version 10.0.1 build-1379776 ) 
    user has to be member of vboxusers
3. install [Packer][1] (version Packer v0.5.2)
4. generate ssh-key 
5. run packer


### generating ssh-key 

e.g. run the below or use scripts/create-packer-ssh-key.sh and move the private key to key/ and the public key to http/

    :::Shell    
    rm -f key/id_rsa_packer_images.private
    ssh-keygen -q -C "packer image creation ssh key" -f key/id_rsa_packer_images.private
    mv key/id_rsa_packer_images.private.pub http/id_rsa_packer_images.pub

### running packer

start the build by running

    :::Shell
    packer build packer-opensuse-12.3-baseinstall.json

this will take quite some time to download the openSuSE iso as wel las the VirtualBox additions iso.

In case you have either VMware or VirtualBox but not both, you can run the single build:

    :::Shell
    packer build -only=vmware-openSUSE-12.3 packer-opensuse-12.3-baseinstall.json

To override the iso to a local file  

    :::Shell
    packer build -var "iso_url=/some/path/openSUSE-12.3-DVD-x86_64.iso" packer-opensuse-12.3-baseinstall.json
    

## Todo :

* remove some rpms that are not used (e.g. wallpaper)
* add some rpms like "make" etc
* run update (or run that as part of another template)
* maybe add preparation scripts to call create ssh-keys and start packer
* confirm that VirtualBox additions are not required or add them


[1]: http://www.packer.io        "Packer"
[2]: http://www.virtualbox.org   "VirtualBox"
[3]: http://www.vmware.com/au/products/workstation  "VMWare Workstation"

